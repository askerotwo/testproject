package com.example.trainproject;

public class Book {
    private String writer;
    private String name;
    private int year;
    private int countOfPages;
    private String shortDescription;

    public Book(String writer, String name, int year, int countOfPages, String shortDescription) {
        this.writer = writer;
        this.name = name;
        this.year = year;
        this.countOfPages = countOfPages;
        this.shortDescription = shortDescription;
    }
    public Book() {
    }

    public String getWriter() {

        return writer;
    }

    public void setWriter(String writer) {

        this.writer = writer;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public int getCountOfPages() {

        return countOfPages;
    }

    public void setCountOfPages(int countOfPages) {

        this.countOfPages = countOfPages;
    }

    public String getShortDescription() {

        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {

        this.shortDescription = shortDescription;
    }
}
