package com.example.trainproject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArrayManipulator {

    private int[] array;

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int getWeightedRandomNumber(int[] weigth) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < weigth[i]; j++) {
                list.add(array[i]);
            }
        }
        Random random = new Random();
        int value = random.nextInt(list.size());
        return list.get(value);
    }

    private void swapInt(int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    public int[] sortArray() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swapInt(j, j + 1);
                }
            }
        }
        return array;
    }

    public int getRandomNumber() {
        Random random = new Random();
        int value = random.nextInt(array.length);
        return array[value];
    }

    public int[] reverseArray() {
        for (int i = 0; i < array.length / 2; i++) {
            swapInt(i, array.length - i - 1);
        }
        return array;
    }
}



