package com.example.trainproject;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class TrainProjectApplicationTests {

    @Test
    public void sort_array() {
        //Два массива - один отсортированный, а второй надо отсортировать
        int [] sortedArray = new int[]{1, 2, 4, 7, 8, 12, 15, 20, 28};
        int [] array = new int[] {28, 15, 7, 20, 1, 12, 2, 8, 4};

        ArrayManipulator arrayManipulator = new ArrayManipulator();
        arrayManipulator.setArray(array);
        arrayManipulator.sortArray();

        Assertions.assertArrayEquals(sortedArray, array);
    }

    @Test
    public void reverse_array() {
        //Два массива - один отсортированный, а второй надо отсортировать
        int [] reversedArray = new int[]{1, 2, 4, 7, 8, 12, 15, 20, 28};
        int [] array = new int[] {28, 20, 15, 12, 8, 7, 4, 2, 1};


        ArrayManipulator arrayManipulator = new ArrayManipulator();
        arrayManipulator.setArray(array);
        arrayManipulator.reverseArray();

        Assertions.assertArrayEquals(reversedArray, array);
    }

    @Test
    public void get_random_number_array() {
        int [] array = new int[] {28, 20, 15, 12, 8, 7, 4, 2, 1};

        ArrayManipulator arrayManipulator = new ArrayManipulator();
        arrayManipulator.setArray(array);
        int randomNumber = arrayManipulator.getRandomNumber();

        Assertions.assertTrue(Arrays.stream(array).anyMatch(el -> el == randomNumber));
    }
}
